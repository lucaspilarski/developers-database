import mysql from "mysql";
import dotenv from "dotenv";

dotenv.config();
export default mysql.createPool({
  host: process.env.HOST,
  user: process.env.USERNAME,
  password: process.env.PASSWORD,
  port: 3306,
  database: "heroku_98b34bf0baa2e48",
});
