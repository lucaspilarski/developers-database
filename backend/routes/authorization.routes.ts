import express, { ErrorRequestHandler } from "express";
const router = express.Router();
import bcrypt from "bcryptjs";
const JWT_KEY = "jwtactive987";
import jwt from "jsonwebtoken";
import { database } from "../config";

router.route("/activate/:token").get((req, res) => {
  const token = req.params.token;
  if (token) {
    jwt.verify(token, JWT_KEY, (err: any, decodedToken: any) => {
      if (err) {
        console.log(err);
      } else {
        const {
          id,
          firstName,
          lastName,
          email,
          nickname,
          password,
          loggedIn,
          registerDate,
        } = decodedToken;
        const putUserToDBString = (hashedPassword: string) =>
          `INSERT INTO ${database}.users (id, firstName, lastName, email, nickname, password, loggedIn, registerDate) 
            VALUES ('${id}', '${firstName}', '${lastName}', '${email}', '${nickname}', '${hashedPassword}', '${loggedIn}', '${registerDate}')`;

        bcrypt.genSalt(10, async (err, salt) => {
          await bcrypt.hash(password, salt, (err, hash) => {
            if (err) throw err;
            req.app.get("db").query(putUserToDBString(hash), function () {
              res.status(201).json("Hurry! User is now registered.");
            });
          });
        });
      }
    });
  } else {
    console.log("Account activation error!");
  }
});

export default router;
