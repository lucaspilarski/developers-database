import express, { ErrorRequestHandler } from "express";
import { database } from "../config";
const router = express.Router();

router.route("/comments").get((req, res) => {
  req.app
    .get("db")
    .query(
      `SELECT * FROM ${database}.comments`,
      function (err: ErrorRequestHandler, result: []) {
        try {
          res.json(result);
        } catch (err) {
          res.status(500).json(err);
        }
      }
    );
});

router.route("/comments/add").post((req, res) => {
  const id = req.body.id;
  const author = req.body.author;
  const text = req.body.text;

  req.app
    .get("db")
    .query(
      `INSERT INTO ${database}.comments (id, author, text) VALUES ('${id}', '${author}', '${text}')`,
      function () {
        try {
          res.json("New comment was sucessfully added!");
        } catch (err) {
          res.status(500).json(err);
        }
      }
    );
});

router.route("/comments/delete").delete((req, res) => {
  const id = req.body.id;

  req.app
    .get("db")
    .query(`DELETE FROM ${database}.comments WHERE id = '${id}'`, function () {
      try {
        res.json("Comment was sucessfully deleted!");
      } catch (err) {
        res.status(500).json(err);
      }
    });
});

export default router;
