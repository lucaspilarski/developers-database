import { Response, Request, ErrorRequestHandler } from "express";
import { database } from "../config";
import express from "express";
const router = express.Router();

router.route("/projects").get((req: Request, res: Response) => {
  req.app
    .get("db")
    .query(
      `SELECT * FROM ${database}.projects`,
      function (err: ErrorRequestHandler, projects: any) {
        req.app
          .get("db")
          .query(
            `SELECT * FROM ${database}.projectstatuses`,
            function (err: ErrorRequestHandler, statuses: any) {
              const result = [];
              for (const project of projects) {
                for (const status of statuses) {
                  if (project.status === status.id) {
                    project.status = status.name;
                    result.push(project);
                  }
                }
              }
              try {
                res.json(result);
              } catch (err) {
                res.status(500).json(err);
              }
            }
          );
      }
    );
});

router.route("/projects/add").post((req: Request, res: Response) => {
  const id = req.body.id;
  const name = req.body.name;
  const company = req.body.company;
  const datefrom = req.body.datefrom === 0 ? null : req.body.datefrom;
  const dateto = req.body.dateto === 0 ? null : req.body.dateto;
  const status = req.body.status;

  const putProjectsToDB = `INSERT INTO ${database}.projects (id, name, company, datefrom, dateto, status) VALUES ('${id}', '${name}', '${company}', '${datefrom}', '${dateto}', '${status}')`;

  req.app.get("db").query(putProjectsToDB, function () {
    try {
      res.json("New project was sucessfully added!");
    } catch (err) {
      res.status(500).json(err);
    }
  });
});

router.route("/projects/delete").delete((req: Request, res: Response) => {
  const id = req.body.id;

  req.app
    .get("db")
    .query(`DELETE FROM ${database}.projects WHERE id = '${id}'`, function () {
      try {
        res.json("Project was sucessfully deleted!");
      } catch (err) {
        res.status(500).json(err);
      }
    });
});

export default router;
