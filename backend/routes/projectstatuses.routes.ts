import express, { ErrorRequestHandler } from "express";
import { database } from "../config";
const router = express.Router();

router.route("/projectstatuses").get((req, res) => {
  req.app
    .get("db")
    .query(
      `SELECT * FROM ${database}.projectstatuses`,
      function (err: ErrorRequestHandler, result: any) {
        try {
          res.json(result);
        } catch (err) {
          res.status(500).json(err);
        }
      }
    );
});

export default router;
