import express, { ErrorRequestHandler } from "express";
import { database } from "../config";
const router = express.Router();

router.route("/skills").get((req, res) => {
  req.app
    .get("db")
    .query(
      `SELECT * FROM ${database}.skills`,
      function (err: ErrorRequestHandler, skills: any) {
        req.app
          .get("db")
          .query(
            `SELECT * FROM ${database}.skillscategories`,
            function (err: ErrorRequestHandler, categories: any) {
              const result = [];
              for (const skill of skills) {
                for (const category of categories) {
                  if (skill.category === category.id) {
                    skill.category = category.name;
                    result.push(skill);
                  }
                }
              }
              try {
                res.json(result);
              } catch (err) {
                res.status(500).json(err);
              }
            }
          );
      }
    );
});

router.route("/skills/add").post((req, res) => {
  const id = req.body.id;
  const name = req.body.name;
  const rate = req.body.rate;
  const category = req.body.category;

  req.app
    .get("db")
    .query(
      `INSERT INTO ${database}.skills (id, name, rate, category) VALUES ('${id}', '${name}', '${rate}', '${category}')`,
      function () {
        try {
          res.json("New skill was sucessfully added!");
        } catch (err) {
          res.status(500).json(err);
        }
      }
    );
});

router.route("/skills/delete").delete((req, res) => {
  const id = req.body.id;

  req.app
    .get("db")
    .query(`DELETE FROM ${database}.skills WHERE id = '${id}'`, function () {
      try {
        res.json("Skill was sucessfully deleted!");
      } catch (err) {
        res.status(500).json(err);
      }
    });
});

export default router;
