import express, { ErrorRequestHandler } from "express";
import { database } from "../config";
const router = express.Router();

router.route("/skillscategories").get((req, res) => {
  req.app
    .get("db")
    .query(
      `SELECT * FROM ${database}.skillscategories`,
      function (err: ErrorRequestHandler, categories: any) {
        try {
          res.json(categories);
        } catch (err) {
          res.status(500).json(err);
        }
      }
    );
});

export default router;
