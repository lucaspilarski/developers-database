import express, { ErrorRequestHandler } from "express";
import { database } from "../config";
const router = express.Router();

router.route("/students").get((req, res) => {
  req.app
    .get("db")
    .query(
      `SELECT * FROM ${database}.students`,
      function (err: ErrorRequestHandler, result: any) {
        try {
          res.json(result);
        } catch (err) {
          res.status(500).json(err);
        }
      }
    );
});

router.route("/students/add").post((req, res) => {
  const id = req.body.id;
  const firstName = req.body.firstName;
  const lastName = req.body.lastName;
  const photo = req.body.photo;
  const birthDate = req.body.birthDate;
  const practiseInYears = req.body.practiseInYears;
  const skills = req.body.skills;
  const projects = req.body.projects;
  const comments = req.body.comments;

  const putToDBStudentString = `INSERT INTO ${database}.students (id, firstName, lastName, photo, birthDate, practiseInYears, skills, projects, comments) VALUES ('${id}', '${firstName}', '${lastName}', '${photo}', '${birthDate}', '${practiseInYears}', '[${skills}]', '[${projects}]', '[${comments}]')`;

  req.app.get("db").query(putToDBStudentString, function () {
    try {
      res.json("New student was sucessfully added!");
    } catch (err) {
      res.status(500).json(err);
    }
  });
});

router.route("/students/delete").delete((req, res) => {
  const id = req.body.id;

  req.app
    .get("db")
    .query(`DELETE FROM ${database}.students WHERE id = '${id}'`, function () {
      try {
        res.json("Student was sucessfully deleted!");
      } catch (err) {
        res.status(500).json(err);
      }
    });
});

export default router;
