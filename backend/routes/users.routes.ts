import express, { ErrorRequestHandler } from "express";
const router = express.Router();
import { v4 as uuid } from "uuid";
const id: string = uuid();
import bcrypt from "bcryptjs";
const JWT_KEY = "jwtactive987";
import jwt from "jsonwebtoken";
import nodemailer from "nodemailer";
import { database } from "../config";

router.route("/login").get((req, res) => {
  const { nickname, password } = req.body;
  req.app
    .get("db")
    .query(
      `SELECT * FROM ${database}.users WHERE nickname = '${nickname}'`,
      function (err: ErrorRequestHandler, user: any) {
        if (!user) {
          res.status(404).json("Username is not found.");
        } else {
          bcrypt.compare(password, user[0].password).then((isMatch) => {
            if (isMatch) {
              res.send(user[0].id);
            } else {
              res.status(404).json("Incorrect password.");
            }
          });
        }
      }
    );
});

router.route("/register").post((req, res) => {
  const {
    firstName,
    lastName,
    email,
    nickname,
    password,
    loggedIn,
    registerDate,
  } = req.body;
  try {
    req.app
      .get("db")
      .query(
        `SELECT * FROM ${database}.users WHERE nickname = '${nickname}'`,
        function (err: ErrorRequestHandler, userWithSameNick: any) {
          if (userWithSameNick.length !== 0) {
            res.status(400).json("Username is already taken.");
          } else {
            req.app
              .get("db")
              .query(
                `SELECT * FROM ${database}.users WHERE email = '${email}'`,
                function (err: ErrorRequestHandler, userWithSameMail: any) {
                  console.log(userWithSameMail);
                  if (userWithSameMail.length !== 0) {
                    res
                      .status(400)
                      .json(
                        "Email is already registred. Did you forgot your password."
                      );
                  } else {
                    const token = jwt.sign(
                      {
                        id,
                        firstName,
                        lastName,
                        email,
                        nickname,
                        password,
                        loggedIn,
                        registerDate,
                      },
                      JWT_KEY,
                      { expiresIn: "30m" }
                    );
                    const CLIENT_URL = "http://" + req.headers.host;
                    const output = `
                      <h2>Please click on below link to activate your account</h2>
                      <p>${CLIENT_URL}/auth/activate/${token}</p>
                      <p><b>NOTE: </b> The above activation link expires in 30 minutes.</p>
                      `;

                    const transporter = nodemailer.createTransport({
                      service: "gmail",
                      auth: {
                        user: "ekslibrisdatabase@gmail.com",
                        pass: "eks022056fH@",
                      },
                    });

                    const mailOptions = {
                      from: '"EksLibris Developers Database" <ekslibrisdatabase@gmail.com>',
                      to: email,
                      subject: "Account Verification: Developers Database ✔",
                      generateTextFromHTML: true,
                      html: output,
                    };

                    transporter.sendMail(
                      mailOptions,
                      (error: any, info: any) => {
                        if (error) {
                          console.log(error);
                        } else {
                          console.log("Mail sent : %s", info.response);
                        }
                      }
                    );
                  }
                }
              );
          }
        }
      );
  } catch (err) {
    res.status(500).json(err);
  }
});

/*router.route("/users").get((req, res) => {
  req.app
    .get("db")
    .query(
      `SELECT * FROM ${database}.users`,
      function (err: ErrorRequestHandler, result: any) {
        try {
          res.json(result);
        } catch (err) {
          res.status(500).json(err);
        }
      }
    );
});

router.route("/users").delete((req, res) => {
  const id = req.body.id;

  req.app
    .get("db")
    .query(
      `DELETE FROM ${database}.users WHERE id = '${id}'`,
      function () {
        try {
          res.json("User was sucessfully deleted!");
        } catch (err) {
          res.status(500).json(err);
        }
      }
    );
});*/

export default router;
