import express from "express";
const app = express();
import path from "path";
import cors from "cors";
import studentsRouter from "./routes/students.routes";
import projectsRouter from "./routes/projects.routes";
import skillsRouter from "./routes/skills.routes";
import commentsRouter from "./routes/comments.routes";
import skillscategories from "./routes/skillscategories.routes";
import projectstatuses from "./routes/projectstatuses.routes";
import authorization from "./routes/authorization.routes";
import users from "./routes/users.routes";
import db from "./database";
import { MysqlError } from "mysql";

app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "../dist")));
app.use(express.json());
app.set("db", db);

db.getConnection((err: MysqlError) => {
  if (err) {
    console.log(err);
    return;
  }
  console.log("Connection established");
});

app.use("/api", users);
app.use("/api", studentsRouter);
app.use("/api", projectsRouter);
app.use("/api", skillsRouter);
app.use("/api", commentsRouter);
app.use("/api", skillscategories);
app.use("/api", projectstatuses);
app.use("/auth", authorization);

app.use("*", (req, res) => {
  res.sendFile(path.join(__dirname, "../dist/index.html"));
});

app.listen(process.env.PORT || 7000, () => {
  console.log("Server is running on port: 7000");
});
