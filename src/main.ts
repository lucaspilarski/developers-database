import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VModal from "vue-js-modal";
import Multiselect from "vue-multiselect";
import keyboardShortcuts from "./plugins/keyboardShortcuts";

import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faCaretUp,
  faCaretDown,
  faChevronRight,
  faChevronLeft,
  faSortAmountDownAlt,
  faSortAmountDown,
  faExclamationCircle,
  faCheck,
  faTimes,
  faArrowCircleRight,
  faAngleDoubleLeft,
  faAngleDoubleRight,
} from "@fortawesome/free-solid-svg-icons";
import { faClock, faEdit } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(
  faClock,
  faArrowCircleRight,
  faCaretUp,
  faCaretDown,
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faChevronRight,
  faChevronLeft,
  faSortAmountDownAlt,
  faSortAmountDown,
  faExclamationCircle,
  faEdit,
  faCheck,
  faTimes
);
Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component("multiselect", Multiselect);

Vue.config.productionTip = false;
Vue.use(VModal);
Vue.use(keyboardShortcuts, router);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
