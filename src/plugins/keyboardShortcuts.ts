import VueRouter from "vue-router";
import { menuLinks } from "@/utils/menuLinks";
import { KeyboardShortcuts } from "@/store/types";
/* eslint-disable @typescript-eslint/ban-ts-comment */

export default {
  //@ts-ignore
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  install(Vue, router: VueRouter): void {
    const config: KeyboardShortcuts = {
      shortcuts: {
        allIds: [1, 2],
        byId: {
          1: {
            id: 1,
            keyCode: 69, //E//
            modifier: "Ctrl",
            key: "e",
            callback: "goRightOnMenu",
          },
          2: {
            id: 2,
            keyCode: 81, //Q
            modifier: "Ctrl",
            key: "q",
            callback: "goLeftOnMenu",
          },
        },
      },
      methods: {
        goRightOnMenu: (router: VueRouter) => {
          const num = menuLinks.findIndex(
            //@ts-ignore
            (item) => item.component === router.history.current.name
          );
          num === menuLinks.length - 1
            ? router.push({ name: menuLinks[0].component })
            : router.push({ name: menuLinks[num + 1].component });
        },

        goLeftOnMenu: (router: VueRouter) => {
          const num = menuLinks.findIndex(
            //@ts-ignore
            (item) => item.component === router.history.current.name
          );
          num === 0
            ? router.push({ name: menuLinks[menuLinks.length - 1].component })
            : router.push({ name: menuLinks[num - 1].component });
        },
      },
    };

    Vue.prototype.$actionsKeypress = (event: Event) => {
      for (const key in config.shortcuts.byId) {
        if (config.shortcuts.byId[key].modifier) {
          if (
            //@ts-ignore
            event.ctrlKey &&
            //@ts-ignore
            event.keyCode === config.shortcuts.byId[key].keyCode
          ) {
            event.preventDefault();
            config.methods[config.shortcuts.byId[key].callback](router);
          }
        }
      }
    };
  },
};
