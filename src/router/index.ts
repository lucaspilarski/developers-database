import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import ProjectCard from "../views/ProjectCard/ProjectCard.vue";
import SingleSkill from "../views/skills/SingleSkill/SingleSkill.vue";
import StudentCard from "../views/StudentCard/Student.vue";
import Skills from "../views/skills/Skills.vue";
import StudentsWrapper from "../views/Students/StudentsWrapper.vue";
import Projects from "../views/projects/Projects.vue";
import Error404 from "../views/Error404.vue";
import NewStudent from "../views/Students/NewStudent/NewStudent.vue";
import ProjectDetail from "../views/ProjectList/ProjectDetail.vue";
import LearningMaterialsView from "../views/LearningMaterials/LearningMaterialsView.vue";
import MaterialSingleView from "../views/LearningMaterials/MaterialSingleView.vue";
import ReusableComponents from "../views/ReusableComponents.vue";
import Login from "../views/login/Login.vue";
import Register from "../views/login/register/Register.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home,
    },
    {
      path: "/stazysci",
      name: "StudentsWrapper",
      component: StudentsWrapper,
    },
    {
      path: "/stazysci/:id",
      name: "StudentCard",
      component: StudentCard,
      props: true,
    },
    {
      path: "/nowyStazysta",
      name: "NewStudent",
      component: NewStudent,
    },
    {
      path: "/umiejetnosci",
      name: "Skills",
      component: Skills,
    },
    {
      path: "/umiejetnosci/:id",
      name: "SingleSkill",
      component: SingleSkill,
      props: true,
    },
    {
      path: "/projekty",
      name: "Projects",
      component: Projects,
    },
    {
      path: "/projekty/:name/:id",
      name: "ProjectCard",
      component: ProjectCard,
      props: true,
    },
    {
      path: "/projekty/:id",
      name: "ProjectDetail",
      props: true,
      component: ProjectDetail,
    },
    {
      path: "/materialy-do-nauki",
      name: "LearningMaterials",
      component: LearningMaterialsView,
    },
    {
      path: "/materialy-do-nauki/:id",
      name: "SingleMaterial",
      component: MaterialSingleView,
    },
    {
      path: "/komponenty-reuzywalne",
      name: "ReusableComponents",
      component: ReusableComponents,
    },
    {
      path: "/logowanie",
      name: "Login",
      component: Login,
    },
    {
      path: "/rejestracja",
      name: "Register",
      component: Register,
    },
    {
      path: "/*",
      name: "Error404",
      component: Error404,
    },
  ],
});

export default router;
