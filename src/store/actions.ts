import {
  saveNewComment,
  setNewStudentDataPayload,
  DataInStore,
  Comment,
  Project,
  Skills,
  Student,
  SaveEditProject,
} from "./types";
import {
  SAVE_NEW_SKILL,
  SET_NEW_SKILL,
  SAVE_NEW_STUDENT_COMMENT,
  SET_NEW_STUDENT_COMMENT,
  SAVE_NEW_STUDENT_DATA,
  SET_NEW_STUDENT_DATA,
  SET_NEW_COMMENT,
  SAVE_NEW_STUDENT_PROJECT,
  SET_NEW_STUDENT_PROJECT,
  SAVE_NEW_STUDENT,
  SET_NEW_STUDENT,
  SAVE_NEW_PROJECT,
  SET_NEW_PROJECT_COMMENT,
  SET_NEW_PROJECT,
  SET_NEW_SKILL_DATA,
  SAVE_NEW_SKILL_DATA,
  SAVE_NEW_EDIT_PROJECT,
  SET_NEW_EDIT_PROJECT,
  SAVE_NEW_COMMENT,
  SET_NEW_COMMENT_PROJECT,
} from "./consts";
import { Commit } from "vuex";

const actions = {
  newSkill: (
    { state, commit }: { state: DataInStore; commit: Commit },
    payload: Skills
  ) => {
    commit("newSkill", { ...payload });
  },
  [SAVE_NEW_SKILL]: (
    { state, commit }: { state: DataInStore; commit: Commit },
    payload: saveNewComment
  ): void => {
    payload.id = state.skills.allIds.slice(-1)[0] + 1;
    commit(SET_NEW_SKILL, { ...payload });
  },

  [SAVE_NEW_STUDENT_COMMENT]: (
    { state, commit }: { state: DataInStore; commit: Commit },
    payload: saveNewComment
  ): void => {
    payload.id = state.comments.allIds.slice(-1)[0] + 1;
    commit(SET_NEW_STUDENT_COMMENT, { ...payload });
  },

  [SAVE_NEW_STUDENT_DATA]: (
    { commit }: { commit: Commit },
    payload: setNewStudentDataPayload
  ): void => {
    commit(SET_NEW_STUDENT_DATA, payload.editedStudent);
    commit(SET_NEW_COMMENT, payload.newComment);
  },

  [SAVE_NEW_STUDENT_PROJECT](
    { commit }: { commit: Commit },
    payload: Project
  ): void {
    commit(SET_NEW_STUDENT_PROJECT, payload);
  },

  [SAVE_NEW_STUDENT_PROJECT](
    { commit }: { commit: Commit },
    payload: Comment
  ): void {
    commit(SET_NEW_COMMENT, payload);
  },

  [SAVE_NEW_STUDENT]({ commit }: { commit: Commit }, payload: Student): void {
    commit(SET_NEW_STUDENT, payload);
  },

  [SAVE_NEW_PROJECT](
    { commit }: { commit: Commit },
    payload: {
      newComment: Comment;
      newProject: Project;
    }
  ): void {
    commit(SET_NEW_PROJECT_COMMENT, payload.newComment);
    commit(SET_NEW_PROJECT, payload.newProject);
  },
  [SAVE_NEW_SKILL_DATA]: function (
    { commit }: { commit: Commit },
    payload: Skills
  ): void {
    commit(SET_NEW_SKILL_DATA, payload);
  },
  [SAVE_NEW_EDIT_PROJECT](
    { commit }: { commit: Commit },
    payload: SaveEditProject
  ): void {
    commit(SET_NEW_EDIT_PROJECT, payload);
  },
  [SAVE_NEW_COMMENT]({ commit }: { commit: Commit }, payload: Comment): void {
    commit(SET_NEW_COMMENT_PROJECT, payload);
  },
};

export default actions;
