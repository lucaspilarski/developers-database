import {
  DataInStore,
  Comment,
  Project,
  Student,
  SkillsCategory,
  Skills,
} from "./types";
import {
  GET_PROJECTS,
  GET_STUDENTS_FOR_PROJECT,
  GET_SKILLS_CATEGORY,
  GET_STUDENT_BY_ID,
  GET_SKILLS_BY_STUDENT_ID,
  GET_PROJECTS_BY_STUDENT_ID,
  GET_COMMENT_BY_STUDENT_ID,
  GET_SKILL_BY_NAME,
  GET_PROJECT_BY_NAME,
  GET_MATERIAL_COMMENTS,
  GET_STUDENTS_FROM_PROJECT,
  GET_SKILLS_FROM_PROJECT,
  GET_COMMENTS_FROM_PROJECT,
} from "@/store/consts";

const getters = {
  [GET_PROJECTS]: (state: DataInStore) => (): Project[] => {
    return state.projects.allIds.map((item) => state.projects.byId[item]);
  },

  [GET_STUDENTS_FOR_PROJECT]:
    (state: DataInStore) =>
    (id: number): Student[] => {
      return state.students.allIds
        .map((el) => state.students.byId[el])
        .filter((student) => student.projects.indexOf(id) !== -1);
    },

  [GET_SKILLS_CATEGORY]: (state: DataInStore) => (): SkillsCategory[] => {
    return state.skills.categories.allIds.map(
      (id) => state.skills.categories.byId[id]
    );
  },

  [GET_STUDENT_BY_ID]:
    (state: DataInStore) =>
    (id: number): Student => {
      return state.students.byId[id];
    },

  [GET_SKILLS_BY_STUDENT_ID]:
    (state: DataInStore) =>
    (id: number): Skills[] => {
      return state.students.byId[id].skills.map(
        (skills_id) => state.skills.byId[skills_id]
      );
    },

  [GET_PROJECTS_BY_STUDENT_ID]:
    (state: DataInStore) =>
    (id: number): Project[] => {
      return state.students.byId[id].projects.map(
        (project_id) => state.projects.byId[project_id]
      );
    },

  [GET_COMMENT_BY_STUDENT_ID]:
    (state: DataInStore) =>
    (id: number): Comment[] => {
      return state.students.byId[id].comments.map(
        (comment_id) => state.comments.byId[comment_id]
      );
    },

  [GET_SKILL_BY_NAME]:
    (state: DataInStore) =>
    (inputValue: string): Skills[] => {
      return Object.values(state.skills.byId).filter(
        (item) => item.name === inputValue
      );
    },

  [GET_PROJECT_BY_NAME]:
    (state: DataInStore) =>
    (inputValue: string): Project[] => {
      return Object.values(state.projects.byId).filter(
        (item) => item.name === inputValue
      );
    },

  [GET_MATERIAL_COMMENTS]:
    (state: DataInStore) =>
    (inputValue: number[]): Comment[] => {
      let comments = [] as Comment[];
      inputValue.forEach(
        (comment) => (comments = [...comments, state.comments.byId[comment]])
      );
      return comments;
    },
  [GET_STUDENTS_FROM_PROJECT]:
    (state: DataInStore) =>
    (id: number): Array<Student> =>
      Object.values(state.students.byId).filter((student) =>
        student.projects.includes(id)
      ),
  [GET_SKILLS_FROM_PROJECT]:
    (state: DataInStore) =>
    (skillsId: Array<number>): Array<Skills> => {
      return skillsId.map((id) => {
        return {
          id: state.skills.byId[id].id,
          name: state.skills.byId[id].name,
        };
      });
    },
  [GET_COMMENTS_FROM_PROJECT]:
    (state: DataInStore) =>
    (commentsId: Array<number>): Array<Comment> => {
      return commentsId.map((id) => state.comments.byId[id]);
    },
};

export default getters;
