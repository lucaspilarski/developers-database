import Vue from "vue";
import Vuex from "vuex";
import actions from "./actions";
import getters from "./getters";
import { mockData } from "./mockData";
import mutations from "./mutations";

Vue.use(Vuex);

export default new Vuex.Store({
  state: mockData,
  mutations: mutations,
  actions: actions,
  getters: getters,
  modules: {},
});
