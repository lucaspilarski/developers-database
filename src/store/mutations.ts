/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Comment,
  DataInStore,
  Material,
  Project,
  Student,
  Skills,
  saveNewComment,
  saveNewSkill,
  setNewStudentData,
  SaveEditProject,
} from "./types";
import {
  SET_NEW_SKILL,
  SET_NEW_STUDENT_COMMENT,
  SET_NEW_STUDENT_DATA,
  SET_NEW_COMMENT,
  SET_NEW_STUDENT_PROJECT,
  SET_NEW_STUDENT,
  SET_NEW_PROJECT_COMMENT,
  SET_NEW_PROJECT,
  SET_NEW_LEARNING_MATERIAL_DATA,
  SET_NEW_MATERIAL_TAGS,
  SET_NEW_MATERIAL_COMMENT,
  SET_NEW_SKILL_DATA,
  SET_LOADING,
  SET_USER_STATUS,
  SET_NEW_EDIT_PROJECT,
  SET_NEW_COMMENT_PROJECT,
} from "./consts";

const mutations = {
  newSkill(state: DataInStore, payload: Skills) {
    state.skills.allIds.push(payload.id);
    state.skills.byId[payload.id] = {
      id: payload.id,
      name: payload.name,
      category: payload.category,
      description: payload.description,
      rate: payload.rate,
    };
  },
  [SET_NEW_SKILL](state: DataInStore, payload: saveNewSkill): void {
    state.skills.allIds.push(payload.id);
    state.students.byId[payload.studentId].skills.push(payload.id);
    state.skills.byId[payload.id] = {
      id: payload.id,
      name: payload.value,
      category: payload.category,
      description: "",
      rate: payload.rate,
    };
  },

  [SET_NEW_STUDENT_COMMENT](state: DataInStore, payload: saveNewComment): void {
    state.comments.allIds.push(payload.id);
    state.comments.byId[payload.id] = {
      id: payload.id,
      author: payload.author,
      text: payload.text,
    };
    state.students.byId[payload.studentId].comments.push(payload.id);
  },

  [SET_NEW_STUDENT_DATA]: (
    state: DataInStore,
    payload: setNewStudentData
  ): void => {
    if (
      typeof payload.id === "number" &&
      state.students.allIds.includes(payload.id)
    ) {
      state.students.byId[payload.id].firstName = payload.name;
      state.students.byId[payload.id].lastName = payload.lastname;
      state.students.byId[payload.id].birthDate = payload.birthDate;
      state.students.byId[payload.id].practiseInYears = payload.practiseInYears;
      state.students.byId[payload.id].photo = payload.photo;
      state.students.byId[payload.id].skills = payload.skills;
      state.students.byId[payload.id].projects = payload.projects;
      state.students.byId[payload.id].comments = payload.comments;
    }
  },

  [SET_NEW_COMMENT]: (state: DataInStore, payload: saveNewComment): void => {
    if (
      typeof payload.id === "number" &&
      !state.comments.allIds.includes(payload.id)
    ) {
      state.comments.allIds.push(payload.id);
      state.comments.byId[payload.id] = {
        id: payload.id,
        author: payload.author,
        text: payload.text,
      };
    }
  },

  [SET_NEW_STUDENT_PROJECT]: function (
    state: DataInStore,
    payload: Project
  ): void {
    if (payload.from === "") {
      payload.from = null;
    } else if (
      payload.from !== undefined &&
      payload.from !== null &&
      typeof payload.from !== "number"
    ) {
      payload.from = Date.parse(payload.from);
    }
    if (payload.to === "") {
      payload.to = null;
    } else if (
      payload.to !== undefined &&
      payload.to !== null &&
      typeof payload.to !== "number"
    ) {
      payload.to = Date.parse(payload.to);
    }
    state.projects.byId = {
      ...state.projects.byId,
      [payload.id]: payload,
    };
    state.projects.allIds.push(payload.id);
  },

  [SET_NEW_STUDENT]: function (state: DataInStore, payload: Student): void {
    state.students.byId = {
      ...state.students.byId,
      [payload.id]: payload,
    };
    state.students.allIds.push(payload.id);
  },

  [SET_NEW_PROJECT_COMMENT](state: DataInStore, payload: Comment): void {
    if (typeof payload.id === "number") {
      state.comments.allIds.push(payload.id);
      state.comments.byId[payload.id] = {
        id: payload.id,
        author: payload.author,
        text: payload.text,
      };
    }
  },

  [SET_NEW_PROJECT](state: DataInStore, payload: Project): void {
    if (typeof payload.id === "number") {
      state.projects.allIds.push(payload.id);
      state.projects.byId[payload.id] = {
        id: payload.id,
        name: payload.name,
        company: payload.company,
        from: payload.from,
        status: payload.status,
        participants: payload.participants,
        description: payload.description,
        comments: payload.comments,
      };
      if (Array.isArray(payload.participants))
        payload.participants.forEach((el) => {
          state.students.byId[el].projects.push(payload.id);
        });
    }
  },

  [SET_NEW_LEARNING_MATERIAL_DATA](
    state: DataInStore,
    payload: Material
  ): void {
    if (!state.learningMaterials.allIds.includes(payload.id)) return;

    ["title", "link", "type", "usefulness"].forEach((key) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      state.learningMaterials.byId[payload.id][key] = payload[key];
    });
  },

  [SET_NEW_MATERIAL_TAGS](
    state: DataInStore,
    { id, tags }: { id: number; tags: string[] }
  ): void {
    state.learningMaterials.byId[id].tags = tags;
  },

  [SET_NEW_MATERIAL_COMMENT](
    state: DataInStore,
    {
      id,
      comment,
    }: {
      id: number;
      comment: string;
    }
  ): void {
    const commentId = state.comments.allIds.length + 1;
    const min = 1;
    const max = state.students.allIds.length;
    const randomAuthorId = Math.floor(Math.random() * (max - min)) + min;

    state.learningMaterials.byId[id].comments.push(commentId);
    state.comments.allIds.push(commentId);
    state.comments.byId[commentId] = {
      id: commentId,
      author: `${state.students.byId[randomAuthorId].firstName} ${state.students.byId[randomAuthorId].lastName}`,
      text: comment,
    };
  },
  [SET_NEW_SKILL_DATA](state: DataInStore, payload: Skills): void {
    for (const skill in state.skills.byId) {
      if (state.skills.byId[skill].id == payload.id) {
        state.skills.byId[skill] = payload;
      }
    }
  },
  [SET_LOADING](state: DataInStore, payload: boolean): void {
    state.loader.show = payload;
  },
  [SET_USER_STATUS]: (state: DataInStore, payload: boolean): void => {
    state.user.loggedIn = payload;
  },
  [SET_NEW_EDIT_PROJECT](state: DataInStore, payload: SaveEditProject): void {
    const { projects } = state;

    const { id, name, description, company, from } = payload;
    projects.byId[id] = {
      ...projects.byId[id],
      name,
      description,
      company,
      from: new Date(from).getTime(),
    };
  },
  [SET_NEW_COMMENT_PROJECT](
    state: DataInStore,
    {
      id,
      projectId,
      text,
      author,
    }: { id: number; projectId: number; text: string; author: string }
  ): void {
    const { comments, projects } = state;
    comments.byId[id] = { id, text, author };
    comments.allIds.push(id);
    projects.byId[projectId].comments?.push(id);
  },
};

export default mutations;
