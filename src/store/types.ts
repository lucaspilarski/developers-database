import VueRouter from "vue-router";

export type funcType = () => void;
export type Student = {
  id: number;
  firstName: string;
  lastName: string;
  photo: string;
  birthDate: number;
  practiseInYears: number;
  skills: Array<number>;
  projects: Array<number>;
  comments: Array<number>;
};
export type Skills = {
  id: number;
  name: string;
  category?: number;
  description?: string;
  rate?: number | string;
};
export type SkillsCategory = {
  id: number;
  name: string;
};
export type Project = {
  id: number;
  name: string;
  company: string;
  from: number | undefined | string | null;
  to?: number | undefined | string | null;
  status: number | string;
  description?: string;
  participants?: Array<number>;
  comments?: Array<number>;
  skills?: Array<number>;
};
export type Comment = {
  id: number;
  author: string;
  text: string;
};
export type Material = {
  id: number;
  title: string;
  link: string;
  tags: string[];
  usefulness: 1 | 2 | 3 | 4 | 5;
  comments: number[];
  type: number;
  // 1: "blog";
  // 3: "docs";
  // 5: "facebook";
  // 6: "repository";
  // 4: "tweet";
  // 2: "video";
};
export interface User {
  id: string;
  password: string;
  nickname: string;
  email: string;
  firstName: string;
  lastName: string;
  loggedIn: boolean;
}
export interface DataInStore {
  students: {
    allIds: number[];
    byId: {
      [key: number]: Student;
    };
  };
  skills: {
    allIds: number[];
    byId: {
      [key: number]: Skills;
    };
    categories: {
      allIds: number[];
      byId: {
        [key: number]: SkillsCategory;
      };
    };
  };
  projects: {
    allIds: number[];
    byId: {
      [key: number]: Project;
    };
    statuses: Array<{ id: number; name: string }>;
  };
  comments: {
    allIds: number[];
    byId: {
      [key: number]: Comment;
    };
  };
  learningMaterials: {
    materialTypes: {
      [key: number]: string;
    };
    allIds: number[];
    byId: {
      [key: number]: Material;
    };
  };
  loader: {
    show: boolean;
  };
  user: User;
}
export interface KeyboardShortcuts {
  shortcuts: {
    allIds: Array<number>;
    byId: {
      [key: number]: Shortcut;
    };
  };
  methods: {
    [key: string]: (router: VueRouter) => void;
  };
}
interface Shortcut {
  id: number;
  keyCode: number;
  modifier: string;
  key: string;
  callback: string;
}

export interface setNewStudentDataPayload {
  newComment: saveNewComment;
  editedStudent: setNewStudentData;
}
export interface saveNewComment {
  studentId: number;
  id: number;
  author: string;
  text: string;
}
export interface saveNewSkill {
  studentId: number;
  id: number;
  rate: string;
  category: number;
  value: string;
}
export interface setNewStudentData {
  birthDate: number;
  comments: number[];
  id: number;
  lastname: string;
  name: string;
  photo: string;
  practiseInYears: number;
  projects: number[];
  skills: number[];
}

export interface SaveEditProject {
  id: number;
  name: string;
  description: string;
  company: string;
  from: number;
  commentId: number;
}
