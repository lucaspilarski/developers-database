export const changeFormatDate = (milsec: number): string => {
  return new Date(milsec).toLocaleDateString();
};
