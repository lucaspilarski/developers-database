export const menuLinks = [
  { component: "Home", text: "Home" },
  { component: "StudentsWrapper", text: "Stażyści" },
  { component: "Skills", text: "Umiejętności" },
  { component: "Projects", text: "Projekty" },
  { component: "LearningMaterials", text: "Materiały do nauki" },
];
