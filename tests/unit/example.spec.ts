import { shallowMount } from "@vue/test-utils";
import HomeView from "@/components/layout/HomeView.vue";

describe("MainView.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(HomeView, {
      propsData: { msg },
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
